#include "fondos.h"

Fondos::Fondos(QString nom)
{
    img.load(":/Fondos/Fondos/"+nom+".png");

}

QRectF Fondos::boundingRect() const
{
    return QRectF(0,0,1000,500);
}

void Fondos::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(0,0,1000,500,img);
}
