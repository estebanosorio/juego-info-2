#include "bulletenemy.h"
#include <QDebug>
#include "game.h"

extern Game *game;

BulletEnemy::BulletEnemy(QGraphicsItem *parent): QObject(), QGraphicsRectItem(parent)
{
    //dibuja el rect
    setRect(0,0,10,10);
    //parametros iniciales
    vx = 50;
    vy = 20;
}

float BulletEnemy::getPx() const
{
    return px;
}

void BulletEnemy::setPx(float value)
{
    px = value;
}

float BulletEnemy::getPy() const
{
    return py;
}

void BulletEnemy::setPy(float value)
{
    py = value;
}

float BulletEnemy::getVx() const
{
    return vx;
}

void BulletEnemy::setVx(float value)
{
    vx = value;
}

float BulletEnemy::getVy() const
{
    return vy;
}

void BulletEnemy::setVy(float value)
{
    vy = value;
}

float BulletEnemy::getAx() const
{
    return ax;
}

void BulletEnemy::setAx(float value)
{
    ax = value;
}

float BulletEnemy::getAy() const
{
    return ay;
}

void BulletEnemy::setAy(float value)
{
    ay = value;
}

void BulletEnemy::move()
{
    //si el bullet colisiona con el enemigo, lo destuye
    QList<QGraphicsItem *> colliding_item1 = collidingItems();
    for (int i = 0, n = colliding_item1.size(); i < n; ++i){
        if(typeid(*(colliding_item1[i])) == typeid(MyRect)){
            game->rect->setVida((game->rect->getVida())-poder);
            if((game->rect->getVida()) == 0){
                scene()->removeItem(colliding_item1[i]);
                delete colliding_item1[i];
            }
            scene()->removeItem(this);
            delete this;
            return;
        }
    }
    act_p();
    setPos(getPx(),getPy());
    if(pos().y() > 500 || pos().x() < 0){
        scene()->removeItem(this);
        delete this;
    }
}

void BulletEnemy::act_a()
{
    ax = 0;
    ay = 9.8;
}

void BulletEnemy::act_v()
{
    vx = vx;
    vy += -ay*dT;
}

void BulletEnemy::act_p()
{
    act_a();
    act_v();
    px -= vx*dT;
    py -= vy*dT+0.5*ay*dT*dT;
}

void BulletEnemy::advance(int ){
    move();
}
