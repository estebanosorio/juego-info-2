#ifndef BULLETENEMY_H
#define BULLETENEMY_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QObject>
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>

#define dT 0.1


class BulletEnemy: public QObject, public QGraphicsRectItem
{
    Q_OBJECT

public:
    BulletEnemy(QGraphicsItem *parent=0);

    float getPx() const;
    float getPy() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;

    void setPx(float value);
    void setPy(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);


public slots:
    void move();

private:
    float px, py, vx, vy, ax, ay;
    int poder =  10;

    void act_a();
    void act_v();
    void act_p();

    void advance(int);
};

#endif // BULLETENEMY_H
