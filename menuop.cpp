#include "menuop.h"
#include "ui_menuop.h"

menuOp::menuOp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::menuOp)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0,1000,500);

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1000,500);

    fondopausa = new Fondos("fondopausa");
    scene->addItem(fondopausa);

}

menuOp::~menuOp()
{
    delete ui;
}
