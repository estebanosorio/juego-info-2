#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>
#include "bullet.h"
/*#include <QGraphicsTextItem>
#include <QFont>*/

#include "bulletenemy.h"

class Enemy: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Enemy();
    ~Enemy();
    int getVida() const;
    void setVida(int value);
    void advance(int);

public slots:
    void move();
    void disparar();

private:
    QTimer* timerDisparar;
    bool flag = true;
    int vida = 100;

    BulletEnemy* bullet2;
};

#endif // ENEMY_H
