#include "enemy.h"

Enemy::Enemy()
{
    //parametros iniciales
    setPos(700,400);
    setRect(0,0,100,100);

    timerDisparar = new QTimer(this);
    connect(timerDisparar, SIGNAL(timeout()),this,SLOT(disparar()));
    //timerDisparar->start(1000);//parametro q cambia de acuerdo con el nivel
    //connect(bullet, SIGNAL(murio()), this, SLOT(~Enemy()));
}

Enemy::~Enemy()
{
    delete bullet2;
}

void Enemy::move()
{
    if(flag){
        setPos(x(),y()-10);
        if(pos().y() == 0)
            flag = false;
    }else{
        setPos(x(),y()+10);
        if(pos().y() == 400)
            flag = true;
    }
}

void Enemy::disparar()
{
    bullet2 = new BulletEnemy();
    bullet2->setPx(x());
    bullet2->setPy(y());
    bullet2->setPos(x(),y());
    scene()->addItem(bullet2);
}

int Enemy::getVida() const
{
    return vida;
}

void Enemy::setVida(int value)
{
    vida = value;
}

void Enemy::advance(int )
{
    move();
}
