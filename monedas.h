#ifndef MONEDAS_H
#define MONEDAS_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>
#include <QGraphicsScene>
#include <QPainter>

class Monedas: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Monedas();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    int getValor() const;
    void setValor(int value);

    QPainterPath shape() const;

public slots:
    void aparecerMonedas();

private:
    int valor = 10;
};

#endif // MONEDAS_H
