#include "bullet.h"
#include "game.h"
#include <QDebug>
#include <QMessageBox>

extern Game *game;

Bullet::Bullet(QGraphicsItem *parent): QObject(), QGraphicsRectItem(parent)
{
    //dibuja el rect
    setRect(0,0,40,40);
    //parametros iniciales
    vx = 50;
    vy = 50;
    avanzar=false;
}

//void Bullet::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
//{
//    QPixmap pixmap;
//    pixmap.load("cuchillo.png");
//    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
//}

void Bullet::move()
{
    //si el bullet colisiona con el enemigo, lo destuye
    QList<QGraphicsItem *> colliding_item = collidingItems();
    for (int i = 0, n = colliding_item.size(); i < n; ++i) {
        if(typeid(*(colliding_item[i]))==typeid(Enemy)){
            game->enemy->setVida((game->enemy->getVida())-poder);
            game->rect->flagbull=true;
            if((game->enemy->getVida()) == 0){
                scene()->removeItem(colliding_item[i]);
                scene()->removeItem(this);
                //delete colliding_item[i];
               // delete this;
                emit murio();
                return;
            }
            scene()->removeItem(this);
            delete this;
            return;
        }
    }
    act_p();
    setPos(getPx(),getPy());
    if(pos().y() > 500 || pos().x() > 1000){
        scene()->removeItem(this);
        delete this;
        game->rect->flagbull=true;
    }
}

bool Bullet::getAvanzar() const
{
    return avanzar;
}

void Bullet::setAvanzar(bool value)
{
    avanzar = value;
    vx=100;
    vy=50;
}

void Bullet::act_a()
{
    ax = 0;
    ay = 9.8;
}

void Bullet::act_v()
{
    vx = vx;
    vy += -ay*dT;
}

void Bullet::act_p()
{
    act_a();
    act_v();
    px += vx*dT;
    py -= vy*dT+0.5*ay*dT*dT;
}

void Bullet::actualizar()
{
    if(avanzar)
    {
        // funcion que hace rotar el arma y avanzar linealmente
        setTransformOriginPoint(QPoint(0,0));
        setRotation(rotation() + rot);
        px += Nav;
        setPos(px,-py);
    }
    else
        // funcion que realiza el movimiento parabolico
        act_p();
}

float Bullet::getVy() const
{
    return vy;
}

void Bullet::setVy(float value)
{
    vy = value;
}

float Bullet::getVx() const
{
    return vx;
}

void Bullet::setVx(float value)
{
    vx = value;
}

float Bullet::getPy() const
{
    return py;
}

void Bullet::setPy(float value)
{
    py = value;
}

float Bullet::getPx() const
{
    return px;
}

void Bullet::setPx(float value)
{
    px = value;
}

float Bullet::getAy() const
{
    return ay;
}

void Bullet::setAy(float value)
{
    ay = value;
}

void Bullet::advance(int ){
    move();
}
