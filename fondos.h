#ifndef FONDOS_H
#define FONDOS_H

#include <QGraphicsItem>
#include <QPainter>
class Fondos : public QGraphicsItem
{
public:
    Fondos(QString nom);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
private:
    QPixmap img;
};

#endif // FONDOS_H
