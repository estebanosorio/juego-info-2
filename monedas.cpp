#include "monedas.h"
#include "stdlib.h"

Monedas::Monedas()
{
    int random_number1 =rand() % 500;
    int random_number2 =rand() % 480;
    setPos(random_number1,480);

}

QRectF Monedas::boundingRect() const
{
    return QRectF(0,0,20,20);
}

void Monedas::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::yellow);
    painter->drawEllipse(boundingRect());
}

int Monedas::getValor() const
{
    return valor;
}

void Monedas::setValor(int value)
{
    valor = value;
}

QPainterPath Monedas::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

void Monedas::aparecerMonedas()
{

}
