#include "trivia.h"
#include "ui_trivia.h"
#include "game.h"

#include <QDebug>

extern Game * game;

Trivia::Trivia(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Trivia)
{
    ui->setupUi(this);
    ui->progressBar->setValue(0);

    ui->label->setText("¿Cuál es el actor que le da vida a Deadpool en la pantalla grande?");
    ui->label->adjustSize();
    ui->label_3->setText("¿En que año fué estrenada la primer pelicula de Deadpool?");
    ui->label_3->adjustSize();
    ui->label_2->setText("¿Cuál es el nombre de Deadpool?");
    ui->label_2->adjustSize();

    connect(ui->pushButton,SIGNAL(pressed()),this,SLOT(on_pushButton_clicked()));
}

Trivia::~Trivia()
{
    delete ui;
}

void Trivia::on_pushButton_clicked()
{
    if(ui->checkBox_3->isChecked()&&ui->checkBox_7->isChecked()&&ui->checkBox_5->isChecked())
    {
        ui->label_4->setText("Respuestas correctas");
        ui->progressBar->setValue(100);

        qDebug() << "Presionado";

        game = new Game();
        game->showMaximized();
        this->close();

    }
    else
    {
        ui->label_4->setText("Respuestas incorrectas");
    }
}
