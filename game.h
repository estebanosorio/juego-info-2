#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>
#include <QKeyEvent>
#include <QLineEdit>
#include <QMessageBox>

#include "myrect.h"
#include "enemy.h"
#include "bloques.h"
#include "monedas.h"
#include "menuop.h"
#include "puntaje.h"
#include "fondos.h"
#include "botones.h"
#include "dbjuego.h"

#include <QObject>

#define PLAY true
#define PAUSE false

class Game: public QGraphicsView
{
    Q_OBJECT
public:
    Game(QWidget * =0);

    QGraphicsScene *scene;
    MyRect *rect, *loadRect;
    Enemy *enemy;
    Bloques *bloques;
    QList<Bloques*> bloques2;
    QTimer *timer;
    QTimer *timer2;
    QTimer *timerGeneral;

    puntaje *pnt;

    Monedas *moneda;
    int contMonedas = 0;


    unsigned int getLevel() const;
    void setLevel(unsigned int value);

    void keyPressEvent(QKeyEvent * event);
    void mousePressEvent(QMouseEvent *m);

public slots:
    void menuInicial();
    void menuIngreso();
    void menuPartida(void);
    void createLevel();
    void ponerBloques();
    void ponerMonedas();
    void onSignin();
    void onLogin();
    void newLevel();

private:
    int level;
    bool playPause;
    menuOp menu;
    QString user;
    QLineEdit *txtRegistro, *txtIngreso;
    DBJuego *dbjuego;
    Fondos* fondopausa;

    void menuPause(bool status);

    //Menu
    //Botones *bSignin;
};

#endif // GAME_H
