#ifndef MENUOP_H
#define MENUOP_H

#include <QWidget>
#include <QGraphicsScene>
#include "fondos.h"

namespace Ui {
class menuOp;
}

class menuOp : public QWidget
{
    Q_OBJECT

public:
    explicit menuOp(QWidget *parent = 0);
    ~menuOp();

private:
    Ui::menuOp *ui;
    QGraphicsScene* scene;
    Fondos* fondopausa;
};

#endif // MENUOP_H
