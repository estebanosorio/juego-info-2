#ifndef BLOQUES_H
#define BLOQUES_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>
#include <QGraphicsScene>

class Bloques: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Bloques();
public slots:
    void movebloques();

private:
    QTimer *timer;

};

#endif // BLOQUES_H
