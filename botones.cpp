#include "botones.h"

Botones::Botones(int _w, int _h)
{
    w=_w;
    h=_h;
}

QRectF Botones::boundingRect() const
{
    return QRectF(0,0,w,h);
}

void Botones::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen;
    //pen.setStyle(Qt::NoPen);
    pen.setColor(Qt::red);
    painter->setPen(pen);
    painter->drawRect(boundingRect());
}

void Botones::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    emit click();
}
