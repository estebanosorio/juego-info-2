#ifndef BULLET_H
#define BULLET_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QObject>
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <QPainter>

#define dT 0.1

class Bullet: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Bullet(QGraphicsItem *parent=0);

    float getPx() const;
    float getPy() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;
    bool getAvanzar() const;

    void setPx(float value);
    void setPy(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);
    void setAvanzar(bool value);

    void actualizar();


    int rot = -1; //rotacion de la pelota
signals:
    void murio();

public slots:
    void move();

private:
    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    float px, py, vx, vy, ax, ay;
    int poder =  50;
    bool avanzar;
    int Nav;

    void act_a();
    void act_v();
    void act_p();

    void advance(int);
};

#endif // BULLET_H
