#ifndef MYRECT_H
#define MYRECT_H

#include <QPainter>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QObject>
#include <QList>
#include <typeinfo>

#include "bullet.h"
#include "bloques.h"

#define dT 0.1
#define G 9.8

class MyRect: public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    MyRect(float _posx, float _posy, float _r, int _vida, int _puntos);
    ~MyRect();
    void keyPressEvent(QKeyEvent * event);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    bool flagjump;
    bool flagbull;

    float getPosx() const;
    float getPosy() const;
    float getRadio() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;
    int getVida() const;
    int getPuntos() const;
    bool getAvanzar() const;

    void setPosx(float value);
    void setPosy(float value);
    void setRadio(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);
    void setVida(int value);
    void setPuntos(int value);
    void setAvanzar(bool value);

    /*****/
    void act_a();
    void act_v();
    void act_p();
    /*****/

    QPainterPath shape() const;

    void colisionMoneda();
    void advance(int);

    bool getSentido() const;
    void setSentido(bool value);

    void disparar();

signals:
    void mate();

public slots:
    //void saltar();
    void colision();
    void smate();

private:
    void checkLimits();

    Bullet *bullet;
    float posx = 0, posy = 460, radio;
    float vx, vy, ax, ay;
    bool sentido;
    int vida;
    int puntos;
    QTimer *timer;
};

#endif // MYRECT_H
