#ifndef PUNTAJE_H
#define PUNTAJE_H

#include <QGraphicsTextItem>

class puntaje : public QGraphicsTextItem
{
public:
    puntaje(QGraphicsItem * =0);

    int getVida() const;
    QString getName() const;

    void setVida(int value);
    void setName(const QString &value);

private:
    int vida = 0;
    QString name;
};

#endif // PUNTAJE_H
