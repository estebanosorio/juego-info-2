#ifndef TRIVIA_H
#define TRIVIA_H

#include <QWidget>
#include "game.h"

namespace Ui {
class Trivia;
}

class Trivia : public QWidget
{
    Q_OBJECT

public:
    explicit Trivia(QWidget *parent = 0);
    ~Trivia();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Trivia *ui;
};

#endif // TRIVIA_H
