#ifndef BOTONES_H
#define BOTONES_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QMouseEvent>

class Botones : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    Botones(int _w, int _h);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *);
signals:
    void click();
private:
    int w,h;
};

#endif // BOTONES_H
