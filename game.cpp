#include "game.h"
#include <QDebug>
#include <QGraphicsLineItem>

Game::Game(QWidget *)
{
    //crear al escena
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1000,500);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1000,500);

    //poner peldaños aleatorios por la escena
    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(ponerBloques()));

    timer2 = new QTimer();
    connect(timer2,SIGNAL(timeout()),this,SLOT(ponerMonedas()));

    timerGeneral = new QTimer();
    connect(timerGeneral,SIGNAL(timeout()),scene,SLOT(advance()));

    dbjuego = new DBJuego();
    rect = NULL;

    txtRegistro = new QLineEdit(this);
    txtIngreso = new QLineEdit(this);
    fondopausa = new Fondos("fondopausa");

    menuInicial();
    //createLevel(level);
}

void Game::menuInicial()
{
    scene->clear();

    Fondos *fondo1 = new Fondos("fondo1");
    scene->addItem(fondo1);

    txtIngreso->hide();


    txtRegistro->setPlaceholderText("NEW USER");
    txtRegistro->setGeometry(660,200,240,25);
    txtRegistro->show();

    Botones *bPlayer = new Botones(150,45);
    bPlayer->setPos(160,160);
    connect(bPlayer,SIGNAL(click()),this,SLOT(menuIngreso()));
    scene->addItem(bPlayer);

    Botones *bMPlayer = new Botones(265,45);
    bMPlayer->setPos(160,230);
    connect(bMPlayer,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bMPlayer);

    Botones *bQuit = new Botones(100,45);
    bQuit->setPos(160,290);
    connect(bQuit,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bQuit);

    Botones *bSignin = new Botones(100,45);
    bSignin->setPos(720,245);
    connect(bSignin,SIGNAL(click()),this,SLOT(onSignin()));
    scene->addItem(bSignin);
}

void Game::mousePressEvent(QMouseEvent *m)
{
    //qDebug()<<"x: "<<m->x()<<" y: "<<m->y();
}


void Game::createLevel(){

    scene->clear();
    txtIngreso->hide();
    Fondos *fondoppal = new Fondos("fondoppal");
    scene->addItem(fondoppal);
    int tiempo;

    //Personaje
    rect = new MyRect(loadRect->getPosx(),loadRect->getPosy(),loadRect->getRadio(),loadRect->getVida(),loadRect->getPuntos());
    rect->setPos(0,460);
    rect->setFlag(QGraphicsItem::ItemIsFocusable);
    rect->setFocus();
    scene->addItem(rect);
    connect(rect,SIGNAL(mate()),this,SLOT(newLevel()));

    delete loadRect;

    //Enemigo
    enemy = new Enemy();
    scene->addItem(enemy);

    //pnt->setName();
    pnt = new puntaje();
    scene->addItem(pnt);

    switch (level) {
    case 1:
        tiempo = 100;
        break;
    case 2:
        tiempo = 80;
        break;
    case 3:
        tiempo = 40;
        break;
    default:
        qDebug() << "fin";
        break;
    }
    timerGeneral->start(tiempo);
    timer->start(6000);
    timer2->start(5000);
    playPause = PLAY;
}

void Game::ponerBloques()
{
    for (int i = 0; i < 4; ++i) {
        bloques = new Bloques();
        scene->addItem(bloques);
    }
}

void Game::ponerMonedas()
{
    moneda = new Monedas();
    scene->addItem(moneda);
    contMonedas++;
    if(contMonedas>=4){
        timer2->stop();
    }
}

unsigned int Game::getLevel() const
{
    return level;
}

void Game::setLevel(unsigned int value)
{
    level = value;
}

void Game::keyPressEvent(QKeyEvent *event)
{
    //Fondos* fondopausa = new Fondos("fondopausa");
    if(event->key() == Qt::Key_P){
        if(playPause==PAUSE){
            timerGeneral->start();
            playPause = PLAY;
            menuPause(1);
            qDebug()<< "start";
        }else{
            timerGeneral->stop();
            playPause = PAUSE;
            menuPause(0);
            qDebug()<< "stop";
            //menu.show();
        }
    }
    if(event->key() == Qt::Key_Left){
        rect->setVx(-30);
        rect->setSentido(false);
    }else if(event->key() == Qt::Key_Right){
        rect->setVx(30);
        rect->setSentido(true);
    }else  if (event->key()==Qt::Key_Up) {
        if(rect->flagjump){
            rect->setVy(50);
            rect->flagjump = false;
        }
    }else if(event->key() == Qt::Key_Space){
        //crear un bullet
        if (rect->flagbull) {
            rect->disparar();
            rect->flagbull = false;
        }

    }else if (event->key()== Qt::Key_R){
        //rect->bullet->setVx(0);

    }else if(event->key()==Qt::Key_E){
        //rect->bullet->setAvanzar(true);
        //        bullet->setPx(getPosx()+10);
        //        bullet->setPos(getPosx(), y());

    }
}

void Game::menuIngreso()
{
    scene->clear();

    Fondos *fondo2 = new Fondos("fondo2");
    scene->addItem(fondo2);

    txtRegistro->hide();


    txtIngreso->setGeometry(300,170,240,25);
    txtIngreso->setPlaceholderText("USER");
    txtIngreso->show();

    Botones *bLogin = new Botones(100,20);
    bLogin->setPos(440,220);
    connect(bLogin,SIGNAL(click()),this,SLOT(onLogin()));
    scene->addItem(bLogin);

    Botones *bBack = new Botones(84,20);
    bBack->setPos(40,16);
    connect(bBack,SIGNAL(click()),this,SLOT(menuInicial()));
    scene->addItem(bBack);
}

void Game::menuPartida(void)
{
    scene->clear();

    Fondos *fondo3 = new Fondos("fondo3");
    scene->addItem(fondo3);

    txtIngreso->hide();

    Botones *bBack = new Botones(82,20);
    bBack->setPos(58,58);
    connect(bBack,SIGNAL(click()),this,SLOT(menuIngreso()));
    scene->addItem(bBack);

    Botones *bNewGame = new Botones(225,20);
    bNewGame->setPos(375,230);
    connect(bNewGame,SIGNAL(click()),this,SLOT(createLevel()));
    scene->addItem(bNewGame);

    Botones *bLoadGame = new Botones(235,20);
    bLoadGame->setPos(365,298);
    connect(bLoadGame,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bLoadGame);

    Botones *bQuit = new Botones(90,20);
    bQuit->setPos(430,375);
    connect(bQuit,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bQuit);

}

void Game::onSignin()
{
    qDebug()<<txtRegistro->text();
    QMessageBox msgBox;
    if(dbjuego->insertar(txtRegistro->text())){
        msgBox.setText("Usuario registrado con exito");
        msgBox.exec();
    }else{
        msgBox.setText("El usuario ya existe, digite uno nuevo");
        msgBox.exec();
    }
    txtRegistro->clear();
}

void Game::onLogin()
{
    qDebug()<<txtIngreso->text();
    QMessageBox msgBox;
    QSqlQuery query;
    query = dbjuego->consultar(txtIngreso->text());


    if(query.value("nombre").toString() == txtIngreso->text()){
        loadRect = new MyRect(0,460,20,query.value("vida").toInt(),query.value("puntaje").toInt());
        level = query.value("nivel").toInt();
        menuPartida();
    }
    else {
        msgBox.setText("Usuario no encontrado, por favor registrese");
        msgBox.exec();
    }
    txtIngreso->clear();
}

void Game::newLevel()
{
    qDebug()<<"s";
    //    timerGeneral->stop();
    //    timer->stop();
    //    timer2->stop();
    //    playPause = PAUSE;

    level++;
    //delete loadRect;
    loadRect = new MyRect(0,460,20,rect->getVida(),rect->getPuntos());
    scene->removeItem(rect);
    createLevel();
}

void Game::menuPause(bool status)
{
    if(status){
        scene->removeItem(fondopausa);
    }else{
        scene->addItem(fondopausa);
    }
}

