#ifndef DBJUEGO_H
#define DBJUEGO_H

#include <QtSql>

class DBJuego
{
public:
    DBJuego();
    enum Campos{
        ID,
        NOMBRE,
        NIVEL,
        PUNTAJE,
        VIDA,
        NUM_CAMPOS // Siempre debe estar al final
    };
private:
    QSqlDatabase baseDatos;
    bool conexionBaseDatos(void);
    bool crearBaseDatos(void);
    bool crearTabla();

public slots:
    bool insertar(QString _name);
    QSqlQuery consultar(QString _name);
};

#endif // DBJUEGO_H
