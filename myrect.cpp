#include "myrect.h"

#include "monedas.h"
#include "game.h"
#include <QDebug>

extern Game * game;

MyRect::MyRect(float _posx, float _posy, float _r, int _vida, int _puntos)
{
    posx = _posx;
    posy = _posy;
    radio = _r;
    vida = _vida;
    puntos = _puntos;
    sentido = true;
    flagjump = true;
    flagbull = true;
}

MyRect::~MyRect()
{
    //delete bullet;
}

void MyRect::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        if(pos().x()>0){
            vx = -50;
            sentido = false;
        }
    }else if(event->key() == Qt::Key_Right){
        if(pos().x()+40<700){
            vx = 50;
            sentido = true;
        }
    }else  if (event->key()==Qt::Key_Up) {
        if(flagjump){
            vy=30;
            flagjump = false;
        }
    }else if(event->key() == Qt::Key_Space){
        //crear un bullet
        bullet = new Bullet();
        bullet->setPx(x()+getRadio());
        bullet->setPy(y());
        bullet->setPos(x()+getRadio(),y());
        scene()->addItem(bullet);

    }else if (event->key()== Qt::Key_R){
        bullet->setVx(0);

    }else if(event->key()==Qt::Key_E){
        bullet->setAvanzar(true);
        //        bullet->setPx(getPosx()+10);
        //        bullet->setPos(getPosx(), y());

    }
}
QRectF MyRect::boundingRect() const
{
    return QRectF(0,0,2*radio,2*radio);
}

void MyRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPixmap pj;
    pj.load(":/Pj/Pj/pj.png");
    painter->drawPixmap(0,0,2*radio,2*radio,pj);
}

float MyRect::getRadio() const
{
    return radio;
}

void MyRect::setRadio(float value)
{
    radio = value;
}

void MyRect::act_a()
{
    ax = 0;

    if(sentido)
        ax = -G;
    else
        ax = G;
    ay = G;
}
void MyRect::act_v()
{
    vx += ax*dT;
    vy += -ay*dT;

    if(vx>-1 && vx<1){
        vx=0;
        ax=0;
    }
}

void MyRect::act_p()
{
    act_a();
    act_v();
    posx += vx*dT+0.5*ax*dT*dT;
    posy -= vy*dT+0.5*ay*dT*dT;
    setPos(posx,posy);

    checkLimits();
}

void MyRect::checkLimits(void){

    if(posy+2*radio>500){
        posy = 460;
        setPos(posx,posy);

        flagjump = true;
    }
    if (posx>600) {
        posx = 600;
        setPos(posx,posy);
    }
    if (posx < 0) {
        posx = 0;
        setPos(posx,posy);
    }
}

bool MyRect::getSentido() const
{
    return sentido;
}

void MyRect::setSentido(bool value)
{
    sentido = value;
}

void MyRect::disparar()
{
    bullet = new Bullet();
    bullet->setPx(x()+getRadio());
    bullet->setPy(y());
    bullet->setPos(x()+getRadio(),y());
    scene()->addItem(bullet);
    connect(bullet,SIGNAL(murio()),this,SLOT(smate()));
}

float MyRect::getVx() const
{
    return vx;
}

void MyRect::setVx(float value)
{
    vx = value;
}

float MyRect::getVy() const
{
    return vy;
}

void MyRect::setVy(float value)
{
    vy = value;
}

float MyRect::getAx() const
{
    return ax;
}

void MyRect::setAx(float value)
{
    ax = value;
}

float MyRect::getAy() const
{
    return ay;
}

void MyRect::setAy(float value)
{
    ay = value;
}

int MyRect::getVida() const
{
    return vida;
}

void MyRect::setVida(int value)
{
    vida = value;
}

int MyRect::getPuntos() const
{
    return puntos;
}

void MyRect::setPuntos(int value)
{
    puntos = value;
}

QPainterPath MyRect::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

void MyRect::colision()
{
    QList<QGraphicsItem *> colliding_item = collidingItems();
    for (int i = 0, n = colliding_item.size(); i < n; ++i) {
        if(typeid(*(colliding_item[i]))==typeid(Bloques)){
            scene()->removeItem(colliding_item[i]);
            delete colliding_item[i];
        }
    }
}

void MyRect::smate()
{
    emit mate();
}

void MyRect::colisionMoneda()
{
    QList<QGraphicsItem *> colliding_item = collidingItems();
    for (int i = 0, n = colliding_item.size(); i < n; ++i) {
        if(typeid(*(colliding_item[i]))==typeid(Monedas)){
            puntos += game->moneda->getValor();
            qDebug()<<puntos;
            scene()->removeItem(colliding_item[i]);
            delete colliding_item[i];
        }
    }
}

float MyRect::getPosy() const
{
    return posy;
}

void MyRect::setPosy(float value)
{
    posy = value;

}

float MyRect::getPosx() const
{
    return posx;
}

void MyRect::setPosx(float value)
{
    posx = value;
}

void MyRect::advance(int ){
    act_p();
    colisionMoneda();
    game->pnt->setVida(game->rect->getPuntos());
}
