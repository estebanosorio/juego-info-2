#include "bloques.h"
#include "stdlib.h"

Bloques::Bloques()
{
    //Posicion aleatoria del enemigo

    int random_number1 =rand() % 500;
    int random_number2 =rand() % 480;
    setPos(random_number1,random_number2);
    setRect(-50,-10,100,20);

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()),this,SLOT(movebloques()));
    timer->start(5000);
}
void Bloques::movebloques()
{
    scene()->removeItem(this);
    delete this;
}
